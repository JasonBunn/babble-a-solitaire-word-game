package edu.westga.cs6241.babble.views;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 * Visual representation of a letter tile as a clickable JButton
 * 
 * @author Jason Bunn
 * @version 2014.11.2
 *
 */
public class TileButton extends JButton{
	private static final long serialVersionUID = 1L;
	private char letter;
	private int index;
	private boolean isWord;
	
	/**
	 * Creates an instance of a TileButton by creating a JButton
	 * with a letter tile Icon based on the letter parameter
	 * 
	 * @param icon		icon created with the corresponding letter
	 * 					tile image file
	 * 
	 * @param letter	letter to be represented by TileButton
	 * 
	 * @param index		current location of TileButton in the 
	 * 					current tile rack
	 */
	public TileButton(ImageIcon icon, char letter, int index) {
		super(icon);
		if (icon == null) {
			throw new IllegalArgumentException("icon must not be null");
		}
		if (letter == '\u0000') {
			throw new IllegalArgumentException("");
		}
		if (index < 0) {
			throw new IllegalArgumentException("index must be a positive int");
		}
		this.letter = letter;
		this.index = index;
		this.isWord = false;
		this.setBorder(BorderFactory.createEmptyBorder());
		this.setFocusable(false);
	}
	 
	/**
	 * Accessor for boolean representation of whether or not
	 * the instance of TileButton is currently a part of the TileButtons 
	 * in the word panel
	 * 
	 * @return		True if TileButton is a part of the current word
	 * 				False if TileButton is not a part of the current word
	 */
	public boolean isWord() {
		return this.isWord;
	}
	
	/**
	 * Sets isWord to boolean parameter given
	 * 
	 * @param isWord	boolean in which to set isWord equal to
	 */
	public void setIsWord(boolean isWord) {
		
		this.isWord = isWord;
	}

	/**
	 * Returns the letter value of the TileButton
	 * 
	 * @return the letter value of the TileButton
	 */
	public char getLetter() {
		return this.letter;
	}
	
	/**
	 * Returns the index assigned to the TileButton
	 * 
	 * @return the index assigned to the TileButton
	 */
	public int getCurrentRackIndex() {
		return this.index;
	}
	 
}