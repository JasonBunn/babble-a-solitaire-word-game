/**
 * 
 */
package edu.westga.cs6241.babble.views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

/**
 * A visual representation of the letter tile rack 
 * 
 * @author Jason Bunn
 * @version 2014.11.2
 *
 */
public class RackPanel extends JPanel implements Observer {
	private static final long serialVersionUID = 1L;
	private String rackString;
	private ArrayList<TileButton> tileIconList;
	private BabbleGUI theGUI;

	/**
	 * Creates an instance of the tile rack
	 * 
	 * @param theGUI	the GUI of the game
	 */
	public RackPanel(BabbleGUI theGUI) {
		if (theGUI == null) {
			throw new IllegalArgumentException("theGUI must not be null");
		}
		this.theGUI = theGUI;
		this.theGUI.addObserver(this);
		
		this.rackString = this.theGUI.getRackString();
		this.tileIconList = new ArrayList<TileButton>();
		
		this.createTileIconList();
		this.displayTileIconList();
	}

	/**
	 * Empties the letter tile list and displays the updated list
	 */
	public void refreshRack() {
		this.tileIconList.clear();
		this.createTileIconList();
	}
	
	/**
	 * Implements the Observer interface
	 */
	@Override
	public void update(Observable arg0, Object arg1) {
		this.removeAll();
		this.displayTileIconList();
	}
	
	/**
	 * Returns the first TileButton in the tileIconList that matches
	 * the given letter
	 * 
	 * @param letter	the letter to be matched with the TileButton
	 * @return			the TileButton that matches the given letter
	 */
	public TileButton searchButtons(char letter) {
		
		for (TileButton theButton : this.tileIconList) {
			if (theButton.getLetter() == Character.toUpperCase(letter)) {
				return theButton;
				
			
			}
		}
		return null;
		
	}

	private void createTileIconList() {
		this.rackString = this.theGUI.getRackString();

		for (int counter = 0; counter < this.rackString.length(); counter++) {
			TileButton currentTile = new TileButton(new ImageIcon(
					"./tileIcons/tile" + this.rackString.charAt(counter)
							+ ".jpg"), this.rackString.charAt(counter), counter);
			currentTile.addActionListener(new TileListener());
			this.tileIconList.add(currentTile);
		}

	}

	private void displayTileIconList() {
		Border borderBevel = BorderFactory.createLoweredBevelBorder();
		TitledBorder border = BorderFactory.createTitledBorder(borderBevel,
				"The Tile Rack");
		border.setTitleJustification(TitledBorder.CENTER);
		this.setBorder(border);

		for (TileButton currentTile : this.tileIconList) {
			this.add(currentTile);
		}
	}

	/*
	 * Defines the listener for ScoreWord button.
	 */
	private class TileListener implements ActionListener {

		/* 
		 *
		 */
		@Override
		public void actionPerformed(ActionEvent e) {

			try {
				AudioInputStream audioInputStream = AudioSystem
						.getAudioInputStream(new File("./Click.wav")
								.getAbsoluteFile());
				Clip clip = AudioSystem.getClip();
				clip.open(audioInputStream);
				clip.start();
			} catch (UnsupportedAudioFileException e1) {
				return;
			} catch (IOException e1) {
				return;
			} catch (LineUnavailableException e1) {
				return;
			}

			ImageIcon clear = new ImageIcon("./tileIcons/tile0.jpg");
			TileButton tile = (TileButton) e.getSource();
			TileButton placeHolder = new TileButton(clear, '0',
					tile.getCurrentRackIndex());
			placeHolder.setOpaque(false);

			if (tile.isWord()) {
				tile.setIsWord(false);
				RackPanel.this.theGUI.removeWordTile(tile);
				RackPanel.this.tileIconList.set(tile.getCurrentRackIndex(),
						tile);
			} else if (!tile.isWord()) {
				tile.setIsWord(true);
				RackPanel.this.theGUI.addWordTile(tile);
				RackPanel.this.tileIconList.set(tile.getCurrentRackIndex(),
						placeHolder);
			}
			RackPanel.this.theGUI.notifyObservers();
		}
	}	

}
