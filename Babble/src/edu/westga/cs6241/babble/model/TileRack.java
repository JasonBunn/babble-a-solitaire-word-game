/**
 * 
 */
package edu.westga.cs6241.babble.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 * Inherits from Tilegroup and represents rack of tiles for
 * player to find words in
 * 
 * @author Jason Bunn
 * @version 2014.10.20
 *
 */
public class TileRack extends TileGroup {
	private int maxSize;

	/**
	 * Creates a tile rack with the given max size
	 * 
	 * @param size max size of tile rack
	 */
	public TileRack(int size) {
		if (size < 1 || size > 98) {
			throw new IllegalArgumentException(
					"Invalid size: Must be between 1 and 98");
		}
		this.maxSize = size;
	}

	/**
	 * Removes the given string of letters from the tile rack
	 * 
	 * @param text	given string of letters to be removed
	 * @return		the Word that was removed from the tile rack
	 */
	public Word removeWord(String text) {
		if (text == null || text.equals("")) {
			throw new IllegalArgumentException(
					"Invalid text: Must contain at least one character");
		}
		ArrayList<Tile> tiles = new ArrayList<Tile>(this.getTiles());
		Word theWord = new Word();
		for (int i = 0; i < text.length(); i++) {
			for (Tile currentTile : tiles) {
				if (currentTile.getLetter() == text.charAt(i)) {
					this.remove(currentTile);
					theWord.append(currentTile);
					break;
				}
			}
		}
		return theWord;
		
	}
	
	/**
	 * Returns the number of tiles needed to refill the tile rack
	 * to max size
	 * 
	 * @return	number of tile needed to fill tile rack
	 */
	public int getNumberOfTilesNeeded() {
		return this.maxSize - this.toString().length();
	}
	
	/**
	 * Returns boolean representing if the given string of letters
	 * can be found in current tile rack
	 * 
	 * @param text	given string of letters to be checked
	 * @return		True if all letters are contained within tile rack
	 * 				False if not all letters are contained within tile rack
	 */
	public boolean canMakeWordFrom(String text) {
		if (text == null || text.equals("")) {
			throw new IllegalArgumentException(
					"Invalid text: Must contain at least one character");
		}
		Word theWord = new Word();
		
		for (int i = 0; i < text.length(); i++) {
			for (Tile currentTile : this.getTiles()) {
				if (currentTile.getLetter() == text.charAt(i)) {
					theWord.append(currentTile);
					break;
				}
			}
		}
		if (theWord.getTiles().size() == text.length()) {
			return true;
		}
		return false;
	}

}
