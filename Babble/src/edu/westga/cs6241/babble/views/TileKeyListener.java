/**
 * 
 */
package edu.westga.cs6241.babble.views;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Key event listener for the application
 * 
 * @author Jason Bunn
 * @version 2014.12.6
 *
 */
public class TileKeyListener implements KeyListener {
	private BabbleGUI theGUI;
	
	/**
	 * Creates an instance of the TileKeyLIstener
	 * 
	 * @param theGUI	the current Babble GUI
	 */
	public TileKeyListener(BabbleGUI theGUI) {
		if (theGUI == null) {
			throw new IllegalArgumentException(
					"Invalid parameter: theGUI must not be null");
		}
		this.theGUI = theGUI;
	}

	/*
	 * Not Used
	 * 
	 * (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyReleased(KeyEvent e) {	
	}

	/*
	 * (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyTyped(KeyEvent e) {
		if (e.getKeyChar() == KeyEvent.VK_ENTER) {
			this.theGUI.scoreWord();
		} else if (e.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
			this.theGUI.backspace();
		}
		TileButton theButton = this.theGUI.addTypedButton(e.getKeyChar());
		if (theButton == null) {
			return;
		}
		theButton.doClick();
	}

	/*
	 * Not Used
	 * 
	 * (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
}
