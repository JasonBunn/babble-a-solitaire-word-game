/**
 * 
 */
package edu.westga.cs6241.babble.views;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Creates the helpDialog JOtionPane to display game rule
 * 
 * @author Jason Bunn
 * @version 2014.07.02
 *
 */
public class BabbleHelp {
	private boolean shouldShowHelpDialog;
	private JFrame theFrame;

	/**
	 * Creates an instance of NimHelpDialog set to true
	 * 
	 * @param theFrame2
	 */
	public BabbleHelp(JFrame theFrame) {
		this.shouldShowHelpDialog = true;
		this.theFrame = theFrame;
	}

	/**
	 * @return the shouldShowHelpDialog
	 */
	public boolean isShouldShowHelpDialog() {
		return shouldShowHelpDialog;
	}

	/**
	 * Return JOptionPane help dialog
	 * 
	 * @return false if false, JOptionPane help dialog if true
	 */
	public boolean showHelpDialog() {
		if (!this.shouldShowHelpDialog) {
			return false;
		}

		JCheckBox shouldShowCheckBox = new JCheckBox(
				"Show this message when starting a new game.", false);

		Object[] msgContent = { this.buildHelpPane(), shouldShowCheckBox };

		JOptionPane.showMessageDialog(this.theFrame, msgContent,
				"Babble Game Instructions", JOptionPane.INFORMATION_MESSAGE);

		return shouldShowCheckBox.isSelected();

	}

	public void showAboutDialog() {

		JOptionPane.showMessageDialog(this.theFrame, this.buildAboutPane(),
				"About", JOptionPane.INFORMATION_MESSAGE);
	}

	private JScrollPane buildAboutPane() {
		String aboutMessage = "Babble (A Solitare Word Game)\n" 
							+ "Authors:  CS6241 + Jason Bunn\n"
							+ "Version:  11/25/2014\n"
							+ "For any questions or concerns please send an email to:\n"
							+ "jbunn5@my.westga.edu";
		JTextArea aboutTextArea = new JTextArea(aboutMessage);

		aboutTextArea.setRows(5);
		aboutTextArea.setColumns(40);
		aboutTextArea.setLineWrap(true);
		aboutTextArea.setWrapStyleWord(true);
		aboutTextArea.setEditable(false);
		aboutTextArea.setOpaque(false);

		JScrollPane aboutPane = new JScrollPane(aboutTextArea);
		return aboutPane;
	}

	private JScrollPane buildHelpPane() {
		String helpMessage = "\nLeft click on a letter tile in the tile rack" 
						   + " or type the corresponding letter on keyboard" 
						   + " to add it to the current word\n"
						   + "\nLeft click on a letter tile in the word panel" 
						   + " or press the backspace key to place the letter" 
						   + " tile back into the tile rack\n"
						   + "\nLeft click the Score Word button or press the" 
						   + " enter key to score the current word\n"
						   + "\nLeft click the Restart Game or press Alt+N to" 
						   + " start a new game\n"
						   + "\nPress Alt+X to exit and close the game window";

		JTextArea helpTextArea = new JTextArea(helpMessage);

		helpTextArea.setRows(13);
		helpTextArea.setColumns(40);
		helpTextArea.setLineWrap(true);
		helpTextArea.setWrapStyleWord(true);
		helpTextArea.setEditable(false);
		helpTextArea.setOpaque(false);

		JScrollPane helpPane = new JScrollPane(helpTextArea);
		return helpPane;
	}

}