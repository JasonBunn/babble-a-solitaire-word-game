/**
 * 
 */
package edu.westga.cs6241.babble.views;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.util.Observable;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import edu.westga.cs6241.babble.controllers.BabbleController;

/**
 * Creates and displays the GUI and Cunducts the flow
 * of the game
 * 
 * @author Jason Bunn
 * @version 2014.11.2
 *
 */
public class BabbleGUI extends Observable {
	private BabbleController controller;
	private JFrame theFrame;
	private Container contentPane;
	private ControlPanel controlPanel;
	private RackPanel rackPanel;
	private WordPanel wordPanel;
	private String candidateWord;
	private String rackString;
	private boolean shouldShowHelpDialog;
	private BabbleHelp help;

	/**
	 * Creates the GUI and creates an instance of the game
	 * 
	 * @param controller    the game engine controller and 
	 * 						interface for the GUI
	 */
	public BabbleGUI(BabbleController controller) {
		if (controller == null) {
			throw new IllegalArgumentException("controller must not be null");
		}
		this.controller = controller;
		this.controller.startGame();
		this.controller.refreshTileRack();
		this.candidateWord = "";
		this.rackString = this.controller.getTilesAsString();
		
		this.theFrame.setDefaultLookAndFeelDecorated(true);

		JDialog.setDefaultLookAndFeelDecorated(true);
		this.help = new BabbleHelp(this.theFrame);

		this.createAndShowGUI();
	}
	
	/*
	 * sets if help should be displayed and displays it if true
	 */
	public void setShouldShowHelpDialog() {
		this.shouldShowHelpDialog = this.help.showHelpDialog();
		
	}
	
	/*
	 * Returns if the help dialog should be displayed
	 */
	public boolean isShouldShowHelpDialog() {
		return this.shouldShowHelpDialog;
		
	}
	
	/*
	 * Displays the About Pane
	 */
	public void showAbout() {
		this.help.showAboutDialog();
	}
	
	/**
	 * Returns this JFrame
	 * 
	 * @return this JFrame
	 */
	public JFrame getFrame() {
		return this.theFrame;
	}
	
	/**
	 * Adds the selected letter tile to the word panel 
	 * tile list
	 * 
	 * @param tile	letter tile to be added
	 */
	public void addWordTile(TileButton tile) {
		if (tile == null) {
			throw new IllegalArgumentException("tile must not be null");
		}
		this.wordPanel.addTileButton(tile);
	}

	/**
	 * Removes the selected letter tile from the word panel 
	 * tile list
	 * 
	 * @param tile	the letter tile to be removed
	 */
	public void removeWordTile(TileButton tile) {
		if (tile == null) {
			throw new IllegalArgumentException("tile must not be null");
		}
		this.wordPanel.removeTileButton(tile);
	}

	/**
	 * Resets the GUI after a word has been scored
	 */
	public void resetBoard() {
		this.candidateWord = "";
		this.rackString = this.controller.getTilesAsString();
		this.wordPanel.clearScoredWord();
		this.rackPanel.refreshRack();
	}

	/**
	 * Returns a string representation of the current candidate word
	 * 
	 * @return a string representation of the current candidate word
	 */
	public String getCandidateWord() {
		return this.candidateWord;
	}

	/**
	 * Returns a string representation of the current tile rack
	 * 
	 * @return a string representation of the current tile rack
	 */
	public String getRackString() {
		return this.rackString;
	}
	
	/*
	 * Scores the current word in the WordPanel
	 */
	public void scoreWord() {
		this.controlPanel.scoreWord();
	}
	
	/*
	 * Removes last TileButton in WordPanel
	 */
	public void backspace() {
		this.wordPanel.removeLastTile();
	}
	
	/**
	 * Returns the specified TileButton from the RackPanel to
	 * be added to the WordPanel
	 * 
	 * @param letter	letter that corresponds to the TileButton
	 * 					to be returned
	 * 
	 * @return			the specified TileButton from the RackPanel to
	 * 					be added to the WordPanel
	 */
	public TileButton addTypedButton(char letter) {
		return this.rackPanel.searchButtons(letter);
	}

	/**
	 * Starts a new game and resets the GUI
	 */
	public void restartGame() {
		try {
			AudioInputStream audioInputStream = AudioSystem
					.getAudioInputStream(new File("./Click.wav")
							.getAbsoluteFile());
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
		} catch (UnsupportedAudioFileException e1) {
			return;
		} catch (IOException e1) {
			return;
		} catch (LineUnavailableException e1) {
			return;
		}
		
		
	    int response = JOptionPane.showConfirmDialog(null,
	    		"Do you want to start a new game?", "Confirm",
	        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
	    if (response == JOptionPane.NO_OPTION) {
	      return;
	    } else if (response == JOptionPane.CLOSED_OPTION) {
	      return;
	    }
		
		this.controller = new BabbleController();
		this.controller.startGame();

		this.controller.refreshTileRack();
		this.candidateWord = "";
		
		this.rackString = this.controller.getTilesAsString();
		
		if (this.shouldShowHelpDialog) {
			this.shouldShowHelpDialog = this.help.showHelpDialog();
		}
		
		this.createPanels();
		this.notifyObservers();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Observable#notifyObservers()
	 */
	@Override
	public void notifyObservers() {
		super.setChanged();
		super.notifyObservers();
		
		this.contentPane.removeAll();
		
		this.candidateWord = this.wordPanel.getCandidateWord();
		
		this.contentPane.add(this.controlPanel, BorderLayout.NORTH);
		this.contentPane.add(this.wordPanel, BorderLayout.WEST);
		this.contentPane.add(this.rackPanel, BorderLayout.SOUTH);
		
		this.theFrame.setFocusable(true);
		
		this.contentPane.revalidate();
		this.contentPane.repaint();
	}
	
	private void createAndShowGUI() {
		this.theFrame = new JFrame("Babble");

		this.theFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Dimension size = new Dimension(600, 475);
		this.theFrame.setPreferredSize(size);

		new BabbleMenuBar(this);

		this.createPanels();

		this.theFrame.pack();
		this.theFrame.setVisible(true);
		this.theFrame.setLocationRelativeTo(null);
		this.theFrame.setFocusable(true);
		this.theFrame.addKeyListener(new TileKeyListener(this));
		
		this.help = new BabbleHelp(this.theFrame);
		this.shouldShowHelpDialog = this.help.showHelpDialog();
	}

	private void createPanels() {
		this.controlPanel = new ControlPanel(this.controller, this);

		this.rackPanel = new RackPanel(this);
		this.rackPanel.setPreferredSize(new Dimension(600, 115));

		this.wordPanel = new WordPanel(this);
		this.wordPanel.setOpaque(false);
		
		this.theFrame.setContentPane(new WordPanelBackground ());
		this.theFrame.setLayout(new BorderLayout());
		
		this.contentPane = this.theFrame.getContentPane();
		this.contentPane.add(this.controlPanel, BorderLayout.NORTH);		
		
		this.contentPane.add(this.rackPanel, BorderLayout.SOUTH);
	}

}
