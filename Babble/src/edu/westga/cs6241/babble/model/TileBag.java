/**
 * 
 */
package edu.westga.cs6241.babble.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * Represents a bag of letter tiles that is used to supply tiles
 * for the tile rack
 * 
 * @author Jason Bunn
 * @version 2014.10.20
 *
 */
public class TileBag {
	private ArrayList<Tile> bag;
	
	/**
	 * Creates an instance of a tile bag
	 */
	public TileBag() {
		this.bag = new ArrayList<Tile>();
		this.fillBag();
	}

	/**
	 * Returns a single tile from the tile bag
	 * 
	 * @return a single tile from the tile bag
	 * @throws EmptyTileBagException 
	 */
	public Tile drawTile() {
		if (this.isEmpty()) {
			throw new IllegalStateException("TileBag is Empty");
		}
		return this.bag.remove(new Random().nextInt(this.bag.size()));
	}
	
	/**
	 * Returns a boolean that represents if the bag has any tiles left
	 * 
	 * @return True if there are no tiles left in the bag
	 * 		   False if there is at least one tile left in the bag
	 */
	public boolean isEmpty() {
		if (this.bag == null || this.bag.size() < 1) {
			return true;
		}
		return false;
	}
	
	/**
	 * Fills the bag with number of each letter tile based on standard
	 * english scrabble distribution
	 */
	private void fillBag() {
		File inFile = new File("./bag.txt");
		Scanner letters = null;
		try {
			letters = new Scanner(inFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		String letterString = "";
		while (letters.hasNextLine()) {
			letterString = letters.nextLine();
			String[] letterSplit = letterString.split(" : ");
			Tile currentTile = 
					new Tile(letterSplit[0].charAt(0), 
							Integer.parseInt(letterSplit[1]));
			this.bag.add(currentTile);
		}
		letters.close();
	}
	
}
