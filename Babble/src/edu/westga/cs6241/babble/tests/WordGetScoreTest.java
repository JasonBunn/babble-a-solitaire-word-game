/**
 * 
 */
package edu.westga.cs6241.babble.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.cs6241.babble.model.Tile;
import edu.westga.cs6241.babble.model.Word;

/**
 * JUnit test class for Word's getScore method
 * 
 * @author Jason Bunn
 * @version 2014.10.27
 *
 */
public class WordGetScoreTest {

	/**
	 * Tests that getScore of Word "HEAVY" is 14
	 */
	@Test
	public void wordHEAVYShouldHaveAScoreOf14() {
		String characters = "HEAVY";
		Word theWord = new Word();
		theWord.append(new Tile(characters.charAt(0), 4));
		theWord.append(new Tile(characters.charAt(1), 1));
		theWord.append(new Tile(characters.charAt(2), 1));
		theWord.append(new Tile(characters.charAt(3), 4));
		theWord.append(new Tile(characters.charAt(4), 4));
		assertTrue(theWord.getScore() == 14);
	}
	
	/**
	 * Tests thst getScore of empty Word is 0
	 */
	@Test
	public void emptyWordShouldHaveAScoreOf0() {
		Word theWord = new Word();
		assertTrue(theWord.getScore() == 0);
	}

}
