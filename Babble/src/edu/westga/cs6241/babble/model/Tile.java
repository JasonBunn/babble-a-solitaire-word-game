/**
 * 
 */
package edu.westga.cs6241.babble.model;

/**
 * Represents a single letter tile containing a letter and
 * point value for that letter
 * 
 * @author Jason Bunn
 * @version 2014.10.20
 *
 */
public class Tile {
	private char letter;
	private int pointValue;
	
	/**
	 * Constructs a single tile with letter and point value
	 * of given parameters
	 * 
	 * @param letter		letter of tile
	 * @param pointValue	point value of tile
	 */
	public Tile(char letter, int pointValue) {
		if (letter <= 0 || Character.isLowerCase(letter)) {
			throw new IllegalArgumentException(
					"Invalid letter: letter must be a valid capital letter");
		}
		if (pointValue < 1 || pointValue > 10) {
			throw new IllegalArgumentException(
					"Invalid pointValue: Must be between 0 and 10");
		}
		this.letter = letter;
		this.pointValue = pointValue;
	}

	/**
	 * Returns the letter of the tile
	 * 
	 * @return	the letter of the tile
	 */
	public char getLetter() {
		return this.letter;
	}

	/**
	 * Returns the point value of the tile
	 * 
	 * @return the point value of the tile
	 */
	public int getPointValue() {
		return this.pointValue;
	}	

}
