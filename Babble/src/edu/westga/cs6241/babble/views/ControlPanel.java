/**
 * 
 */
package edu.westga.cs6241.babble.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import edu.westga.cs6241.babble.controllers.BabbleController;

/**
 * Panel that contains action buttons and status labels for the game
 * Displays current Score and Status and contains buttons to restart
 * game and score the word created from the tile rack
 * 
 * @author Jason Bunn
 * @version 2014.11.2
 *
 */
public class ControlPanel extends JPanel implements Observer {
	private static final long serialVersionUID = 1L;
	private BabbleController controller;
	private BabbleGUI theGUI;
	private JButton btnScoreWord;
	private JButton btnStartGame;
	private JLabel lblScore;
	private JLabel lblFeedback;

	/**
	 * Creates an instance of the ControlPanel with the current 
	 * 
	 * @param controller
	 * @param theGUI
	 */
	public ControlPanel(BabbleController controller, BabbleGUI theGUI) {
		if (controller == null) {
			throw new IllegalArgumentException("controller must not be null");
		}
		if (theGUI == null) {
			throw new IllegalArgumentException("theGUI must not be null");
		}
		this.controller = controller;
		this.theGUI = theGUI;
		this.theGUI.addObserver(this);
		
		this.btnScoreWord = new JButton("Score Word");
		this.btnScoreWord.setBackground(new Color(184, 204, 105));
		
		this.btnStartGame = new JButton("Restart Game");
		this.btnStartGame.setBackground(new Color(102, 200, 232));
		
		this.lblScore = new JLabel();
		this.lblFeedback = new JLabel();
		
		this.addListeners();
		this.createPanel();
	}
	
	/**
	 * Clicks the Scoreword button to score word
	 */
	public void scoreWord() {
		this.btnScoreWord.doClick();
	}
	
	/**
	 * Implements the Observer interface
	 */
	@Override
	public void update(Observable arg0, Object arg1) {
		this.removeAll();
		this.createPanel();
	}
	
	private void addListeners() {
		this.btnStartGame.addActionListener(new StartGameListener());
		this.btnScoreWord.addActionListener(new ScoreWordListener());
	}

	private void createPanel() {	
		this.lblScore.setText("Current Score:    " + Integer.toString(this.controller.getScore()));
		
		
		this.lblFeedback.setText(this.getFeedback());
		
		Font font1 = new Font(null, Font.BOLD, 20);
		Font font2 = new Font(null, Font.BOLD, 22);
		
		Dimension btnSize = new Dimension(115, 75);
		
		this.lblScore.setFont(font2);
		this.lblScore.setHorizontalAlignment(JLabel.CENTER);
		this.lblScore.setVerticalAlignment(JLabel.TOP);
		
		this.lblFeedback.setFont(font1);
		this.lblFeedback.setForeground(new Color(238, 57, 66));
		this.lblFeedback.setHorizontalAlignment(JLabel.CENTER);
		
		this.btnStartGame.setHorizontalAlignment(JButton.CENTER);
		this.btnStartGame.setVerticalAlignment(JButton.CENTER);
		this.btnStartGame.setPreferredSize(btnSize);
		
		this.btnScoreWord.setHorizontalAlignment(JButton.CENTER);
		this.btnScoreWord.setVerticalAlignment(JButton.CENTER);
		this.btnScoreWord.setPreferredSize(btnSize);
		
		this.setLayout(new GridBagLayout());
		
		this.add(new StartPanel());
		this.add(new StatusPanel());
		this.add(new ScorePanel());
	}

	private String getFeedback() {
		String feedback = "";
		if (this.theGUI.getCandidateWord() == "") {
			return "Waiting for word";
		}
		int status;
		try {
			status = this.controller.checkCandidateWord(this.theGUI
					.getCandidateWord());
		} catch (IllegalArgumentException e) {
			status = 0;
		}
		if (status == 0) {
			feedback = "Waiting for word";
		} else if (status == 1) {
			feedback = "Misspelled word. Try again.";
		} else if (status == 2) {
			feedback = "Can not make word from current tiles";
		} 
		return feedback;
	}

	/*
	 * Defines the listener for ScoreWord button.
	 */
	private class ScoreWordListener implements ActionListener {

		/* 
		 *
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			
			
			if (ControlPanel.this.theGUI.getCandidateWord() == "") {
				ControlPanel.this.getFeedback();
				ControlPanel.this.theGUI.notifyObservers();
				return;
			}
			if (ControlPanel.this.controller
					.checkCandidateWord(ControlPanel.this.theGUI
							.getCandidateWord()) == 0) {
				try {
					AudioInputStream audioInputStream = AudioSystem
							.getAudioInputStream(new File("./ding.wav")
									.getAbsoluteFile());
					Clip clip = AudioSystem.getClip();
					clip.open(audioInputStream);
					clip.start();
				} catch (UnsupportedAudioFileException e1) {
					return;
				} catch (IOException e1) {
					return;
				} catch (LineUnavailableException e1) {
					return;
				}
				
				ControlPanel.this.controller
						.removeWord(ControlPanel.this.theGUI.getCandidateWord());
				ControlPanel.this.controller.refreshTileRack();
				ControlPanel.this.theGUI.resetBoard();
				ControlPanel.this.theGUI.notifyObservers();
				ControlPanel.this.lblFeedback.setText("Good Job!");
				ControlPanel.this.repaint();
				
				try {
					AudioInputStream audioInputStream = AudioSystem
							.getAudioInputStream(new File("./Click.wav")
									.getAbsoluteFile());
					Clip clip = AudioSystem.getClip();
					clip.open(audioInputStream);
					clip.start();
				} catch (UnsupportedAudioFileException e1) {
					return;
				} catch (IOException e1) {
					return;
				} catch (LineUnavailableException e1) {
					return;
				}
			}
		}
	}

	/*
	 * Defines the listener for ScoreWord button.
	 */
	private class StartGameListener implements ActionListener {

		/* 
		 *Implements the ActionListener interface
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			
			ControlPanel.this.theGUI.restartGame();
		}
	}
	
	/*
	 * Displays the status labels for the game
	 */
	private class StatusPanel extends JPanel {
		private static final long serialVersionUID = 1L;

		private StatusPanel() {
			Dimension size = new Dimension(330, 75);
			this.setPreferredSize(size);
			Border borderBevel = BorderFactory.createLoweredBevelBorder();
			TitledBorder border = BorderFactory.createTitledBorder(borderBevel,
					"Game Status Panel");
			border.setTitleJustification(TitledBorder.CENTER);
			this.setBorder(border);
			this.setLayout(new BorderLayout());
			if (ControlPanel.this.controller.checkForEndGame()) {
				ControlPanel.this.lblScore.setText("Final Score:    " + Integer.toString(ControlPanel.this.controller.getScore()));
				
			}
			this.add(ControlPanel.this.lblFeedback, BorderLayout.SOUTH);
			this.add(ControlPanel.this.lblScore, BorderLayout.NORTH);
		}
	}
	
	/*
	 * Displays the score word button 
	 */
	private class ScorePanel extends JPanel {
		private static final long serialVersionUID = 1L;

		private ScorePanel() {
			this.add(ControlPanel.this.btnScoreWord);
		}		
	}

	/*
	 * Displays the restart game button
	 */
	private class StartPanel extends JPanel {
		private static final long serialVersionUID = 1L;

		private StartPanel() {
			this.add(ControlPanel.this.btnStartGame);
		}
	}

}
