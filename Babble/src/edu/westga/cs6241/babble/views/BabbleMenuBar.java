/**
 * 
 */
package edu.westga.cs6241.babble.views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

/**
 * @author jason_000
 *
 */
public class BabbleMenuBar extends JMenuBar{
	private static final long serialVersionUID = 1L;
	private BabbleGUI theGUI;
	private JMenu menu;
	
	
	public BabbleMenuBar(BabbleGUI theGUI) {
		this.theGUI = theGUI;
		this.buildMenuBar();
	}

	private void buildMenuBar() {
		this.buildFileMenu();
		this.buildHelpMenu();
		this.theGUI.getFrame().setJMenuBar(this);
	}

	private void buildFileMenu() {
		this.menu = new JMenu("File");
		this.menu.setMnemonic(KeyEvent.VK_F);
		this.menu.getAccessibleContext().setAccessibleDescription("File menu");

		JMenuItem menuItem = new JMenuItem("New game", KeyEvent.VK_N);
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,
				ActionEvent.ALT_MASK));
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BabbleMenuBar.this.theGUI.restartGame();
			}
		});
		this.menu.add(menuItem);

		this.menu.addSeparator();

		menuItem = new JMenuItem("Exit", KeyEvent.VK_X);
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X,
				ActionEvent.ALT_MASK));
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		this.menu.add(menuItem);

		this.add(this.menu);
	}
	
	private void buildHelpMenu() {
		this.menu = new JMenu("Help");
		this.menu.setMnemonic(KeyEvent.VK_H);
		this.menu.getAccessibleContext().setAccessibleDescription("Help menu");
		
		JMenuItem menuItem = new JMenuItem("Help", KeyEvent.VK_D); 
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BabbleMenuBar.this.theGUI.setShouldShowHelpDialog();
			}
		});		
		this.menu.add(menuItem);
		
		this.menu.addSeparator();
		menuItem = new JMenuItem("About", KeyEvent.VK_A); 
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BabbleMenuBar.this.theGUI.showAbout();
			}
		});		
		this.menu.add(menuItem);
		
		this.add(this.menu);
	}
	
}
