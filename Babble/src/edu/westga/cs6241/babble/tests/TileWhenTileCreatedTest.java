package edu.westga.cs6241.babble.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.cs6241.babble.model.Tile;


/**
 * JUnit test class for Tile's constructor
 * 
 * @author Jason Bunn
 * @version 2014.10.20
 *
 */
public class TileWhenTileCreatedTest {

	/**
	 * Test that can create a Tile with 'F' with 4 points is True
	 */
	@Test
	public void canMakeA4PointFTile() {
		char letter = 'F';
		Tile testTile = new Tile(letter, 4);
		assertTrue(testTile.getLetter() == 'F');
		assertTrue(testTile.getPointValue() == 4);
	}
	
	/**
	 * Tests that creating a Tile with 'h' throws IllegalArgumentException
	 */
	@Test(expected = IllegalArgumentException.class)
	public void canNotMakeATileWithLetterh() {
		char letter = 'h';
		new Tile(letter, 4);
	}
	
	/**
	 * Tests that creating a Tile with -4 point value throws
	 * IllegalArgumentException
	 */
	@Test(expected = IllegalArgumentException.class)
	public void canNotMakeATileWithPointValueNegative4() {
		char letter = 'H';
		new Tile(letter, -4);
	}
	
	/**
	 * Tests that creating a Tile with 0 point value throws
	 * IllegalArgumentException
	 */
	@Test(expected = IllegalArgumentException.class)
	public void canNotMakeATileWithPointValue0() {
		char letter = 'H';
		new Tile(letter, 0);
	}

}
