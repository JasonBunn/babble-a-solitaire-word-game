/**
 * 
 */
package edu.westga.cs6241.babble.views;

import java.awt.Graphics;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 * Creates the background for the word panel
 * 
 * @author Jason Bunn
 * @version 2014.11.2
 *
 */
public class WordPanelBackground extends JPanel {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Draws the background image from the scrabbleboard.jpg image file
	 */
	public void paintComponent(Graphics g) {
		g.drawImage(new ImageIcon("./scrabbleBoard.jpg").getImage(),
				0, 85, this.getParent().getWidth(), this.getParent().getHeight() - 215, null);
	}
	
}
