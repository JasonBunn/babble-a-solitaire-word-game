/**
 * 
 */
package edu.westga.cs6241.babble.model;

import java.util.ArrayList;


/**
 * List of tiles to be scored and current score
 * 
 * @author Jason Bunn
 * @version 2014.10.20
 *
 */
public class Word extends TileGroup {
	private int score;
	private ArrayList<Tile> word;
	
	/**
	 * Creates the list of tiles to be scored
	 */
	public Word() {
		this.word = this.getTiles();
		this.score = 0;
	}
	
	/**
	 * Calculates score of current word and adds it to the total score
	 * 
	 * @return	the current score
	 */
	public int getScore() {
		for (Tile currentTile : word) {
			this.score+= currentTile.getPointValue();
		}
		return this.score;
	}
	
}
