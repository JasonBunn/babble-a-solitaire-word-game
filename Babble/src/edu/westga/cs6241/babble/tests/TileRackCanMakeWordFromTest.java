/**
 * 
 */
package edu.westga.cs6241.babble.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.cs6241.babble.model.Tile;
import edu.westga.cs6241.babble.model.TileRack;

/**
 * JUnit test class for TileRack's canMakeWordFrom method
 * 
 * @author Jason Bunn
 * @version 2014.10.20
 *
 */
public class TileRackCanMakeWordFromTest {

	/**
	 * Tests when TileRack "HEAVYHX" Contains "HEAVY" is true
	 */
	@Test
	public void ATileRackShouldContainTheWordHEAVY() {
		String word = "HEAVYHX";
		TileRack rack = new TileRack(7);
		rack.append(new Tile(word.charAt(0), 1));
		rack.append(new Tile(word.charAt(1), 1));
		rack.append(new Tile(word.charAt(2), 1));
		rack.append(new Tile(word.charAt(3), 1));
		rack.append(new Tile(word.charAt(4), 1));
		rack.append(new Tile(word.charAt(5), 1));
		rack.append(new Tile(word.charAt(6), 1));
		assertTrue(rack.canMakeWordFrom("HEAVY"));
	}
	
	/**
	 * Tests when tileRack "HEAVYHX" contains "WET" is False
	 */
	@Test
	public void ATileRackShouldNotContainTheWordWET() {
		String word = "HEAVYHX";
		TileRack rack = new TileRack(7);
		rack.append(new Tile(word.charAt(0), 1));
		rack.append(new Tile(word.charAt(1), 1));
		rack.append(new Tile(word.charAt(2), 1));
		rack.append(new Tile(word.charAt(3), 1));
		rack.append(new Tile(word.charAt(4), 1));
		rack.append(new Tile(word.charAt(5), 1));
		rack.append(new Tile(word.charAt(6), 1));
		assertFalse(rack.canMakeWordFrom("WET"));
	}
	
	/**
	 * Tests when can removeWord "HEAVY from TileRack "HEAVYHX"
	 */
	@Test
	public void shouldBeAbleToRemoveTheWordHeavyFromATileRack() {
		String word = "HEAVYHX";
		TileRack rack = new TileRack(7);
		rack.append(new Tile(word.charAt(0), 1));
		rack.append(new Tile(word.charAt(1), 1));
		rack.append(new Tile(word.charAt(2), 1));
		rack.append(new Tile(word.charAt(3), 1));
		rack.append(new Tile(word.charAt(4), 1));
		rack.append(new Tile(word.charAt(5), 1));
		rack.append(new Tile(word.charAt(6), 1));
		rack.removeWord("HEAVY");
		assertTrue(rack.getNumberOfTilesNeeded() == 5);
		assertFalse(rack.canMakeWordFrom("HEAVY"));
	}
	
	/**
	 * Tests that empty TileRack does not contain "DWELL"
	 */
	@Test
	public void anEmptyTileRackShouldNotContainTheWordDWELL() {
		TileRack rack = new TileRack(7);
		assertFalse(rack.canMakeWordFrom("DWELL"));
	}
	
	/**
	 * Tests that letters are not removed more than once
	 */
	@Test
	public void shouldNotRemoveDuplicateTilesFromATileRack() {
		String word = "CATAAAA";
		TileRack rack = new TileRack(7);
		rack.append(new Tile(word.charAt(0), 1));
		rack.append(new Tile(word.charAt(1), 1));
		rack.append(new Tile(word.charAt(2), 1));
		rack.append(new Tile(word.charAt(3), 1));
		rack.append(new Tile(word.charAt(4), 1));
		rack.append(new Tile(word.charAt(5), 1));
		rack.append(new Tile(word.charAt(6), 1));
		rack.removeWord("CAT");
		assertTrue(rack.canMakeWordFrom("AAAA"));
		assertTrue(rack.getNumberOfTilesNeeded() == 3);
	}
	
	/**
	 * Tests that word can be removed from TileRack when letters
	 * are not in order
	 */
	@Test
	public void canRemoveAWordFromATileRackWhenItsTilesAreOutOfOrder() {
		String word = "TAACAAA";
		TileRack rack = new TileRack(7);
		rack.append(new Tile(word.charAt(0), 1));
		rack.append(new Tile(word.charAt(1), 1));
		rack.append(new Tile(word.charAt(2), 1));
		rack.append(new Tile(word.charAt(3), 1));
		rack.append(new Tile(word.charAt(4), 1));
		rack.append(new Tile(word.charAt(5), 1));
		rack.append(new Tile(word.charAt(6), 1));
		rack.removeWord("CAT");
		assertTrue(rack.canMakeWordFrom("AAAA"));
		assertTrue(rack.getNumberOfTilesNeeded() == 3);
	}

}
