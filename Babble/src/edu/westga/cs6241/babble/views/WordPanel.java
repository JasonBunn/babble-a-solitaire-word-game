/**
 * 
 */
package edu.westga.cs6241.babble.views;

import java.awt.Component;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.Box;
import javax.swing.JPanel;

/**
 * Visual representation of a group of TileButton representing 
 * the current candidate word
 * 
 * @author Jason Bunn
 * @version 2014.11.2
 *
 */
public class WordPanel extends JPanel implements Observer {
	private static final long serialVersionUID = 1L;
	private ArrayList<TileButton> tileList;
	private BabbleGUI theGUI;
	private Component space;

	/**
	 * Creates an instance of a WordPanel based on the current GUI
	 * 
	 * @param theGUI	the given instance of the BabbleGUI
	 */
	public WordPanel(BabbleGUI theGUI) {
		if (theGUI == null) {
			throw new IllegalArgumentException("theGUI must not be null");
		}
		this.theGUI = theGUI;
		this.theGUI.addObserver(this);
		this.tileList = new ArrayList<TileButton>();
		
		
		
		this.space = Box.createHorizontalStrut(55);
		this.displayWord();
	}

	/**
	 * Adds the given TileButton to the list of current 
	 * TileButtons
	 * 
	 * @param tile		TileButton to be added
	 */
	public void addTileButton(TileButton tile) {
		if (tile == null) {
			throw new IllegalArgumentException("tile must not be null");
		}
		this.tileList.add(tile);
	}

	/**
	 * Removes the given TileButton from the list of current 
	 * TileButtons
	 * 
	 * @param tile		TileButton to be removed
	 */
	public void removeTileButton(TileButton tile) {
		if (tile == null) {
			throw new IllegalArgumentException("tile must not be null");
		}
		this.tileList.remove(tile);
	}
	
	/*
	 * Removes last tile in the tile list
	 */
	public void removeLastTile() {
		this.tileList.get(this.tileList.size() - 1).doClick();
	}

	/**
	 * Returns a string representation of the current candidate word
	 * 
	 * @return a string representation of the current candidate word
	 */
	public String getCandidateWord() {
		String candidateWord = "";
		for (TileButton currentTile : tileList) {
			candidateWord += currentTile.getLetter();
		}
		return candidateWord;
	}
	
	/**
	 * Empties the current list of TileButtons
	 */
	public void clearScoredWord() {
		this.tileList.clear();
	}
	
	/**
	 * Implements the Observer interface
	 */
	@Override
	public void update(Observable arg0, Object arg1) {
		this.displayWord();
	}

	private void displayWord() {
		this.removeAll();
		
		this.setLayout(new GridBagLayout());
		this.add(this.space);
		for (TileButton currentTile : tileList) {
			currentTile.setAlignmentY(CENTER_ALIGNMENT);
			this.add(currentTile);
		}	
		this.repaint();
	}
	
}
