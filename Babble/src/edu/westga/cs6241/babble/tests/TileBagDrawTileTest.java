/**
 * 
 */
package edu.westga.cs6241.babble.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.cs6241.babble.model.TileBag;

/**
 * JUnit test class for TileBag's drawTile method
 * 
 * @author Jason Bunn
 * @version 2014.10.20
 *
 */
public class TileBagDrawTileTest {

	
	/**
	 * Test method for {@link edu.westga.cs6241.babble.model.TileBag#drawTile()}.
	 * @throws IllegalStateException 
	 */
	@Test
	public void testCanDrawAllTilesFromATileBag() {
		TileBag theBag = new TileBag();
		for (int i = 0; i < 98; i++) {
			theBag.drawTile();
		}
		assertTrue(theBag.isEmpty());
	}
	
	/**
	 * Test method for {@link edu.westga.cs6241.babble.model.TileBag#drawTile()}.
	 * @throws IllegalStateException 
	 * 
	 */
	@Test(expected = IllegalStateException.class)
	public void testCanNotDraw100TilesFromATileBag() throws IllegalStateException   {
		TileBag theBag = new TileBag();
		for (int i = 0; i < 100; i++) {
				theBag.drawTile();
		}
	}

}
