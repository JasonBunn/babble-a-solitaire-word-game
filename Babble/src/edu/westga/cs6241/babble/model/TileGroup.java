/**
 * 
 */
package edu.westga.cs6241.babble.model;

import java.util.ArrayList;

/**
 * Abstract class used to store and manipulate list of tiles
 * 
 * @author Jason Bunn
 * @version 2014.10.20
 *
 */
public abstract class TileGroup {
	private ArrayList<Tile> tileGroup = new ArrayList<Tile>();

	/**
	 * Adds the given tile to the end of the list of tiles
	 * 
	 * @param aTile		tile to add to list
	 */
	public void append(Tile aTile) {
		if (aTile == null) {
			throw new IllegalArgumentException(
					"Ivalid aTile: Must not be null");
		}
		this.tileGroup.add(aTile);
	}
	
	/**
	 * Removes given tile from the list of tiles
	 * 
	 * @param aTile		tile to be removed
	 */
	public void remove(Tile aTile) {
		if (aTile == null) {
			throw new IllegalArgumentException(
					"Ivalid aTile: Must not be null");
		}
		this.tileGroup.remove(aTile);
	}
	
	/**
	 * Returns a string representation of the list of tiles
	 * 
	 * @return a string representation of the list of tiles
	 */
	public String toString() {
		String theString = "";
		for (Tile currentTile : this.tileGroup) {
			theString += currentTile.getLetter();
		}
		return theString;
	}
	
	/**
	 * Returns the list of tiles
	 * 
	 * @return	the list of tiles
	 */
	public ArrayList<Tile> getTiles() {
		return this.tileGroup;
	}
	
}
