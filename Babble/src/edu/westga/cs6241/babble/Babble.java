package edu.westga.cs6241.babble;

import java.awt.Dimension;

import edu.westga.cs6241.babble.controllers.BabbleController;
import edu.westga.cs6241.babble.views.*;

/**
 * Main class for starting a Babble game. 
 * @author lewisb
 *
 */
public class Babble {

	public static void main(String[] args) throws Exception {
	
		BabbleController mainController = new BabbleController();
		mainController.startGame();
		new BabbleGUI(mainController);
		
	}

}
